#include <bits/stdc++.h>

using namespace std;

struct book
{
        int id, year, pages;
        string autor, publ, name;
};

bool comp(const book & a, const book & b){
        return a.autor < b.autor;
}

void bubble_sort(vector<book> & a){
	for (int i = 0; i < a.size()-1; ++i)
		for(int j = i+1; j < a.size(); j++)
			if(!comp(a[i], a[j])) swap(a[i], a[j]);
}

int main()
{
	int n, k;
	cout << "Skolko knig?\n";
	cin >> n;
	vector<book> a(n);
	for(int i = 0; i < n; i++)
	{
		cout << "Vvedite parametry " << i+1 << " knigi\n";
		cin >> a[i].id >> a[i].autor >> a[i].name >> a[i].year >> a[i].publ >> a[i].pages; 
	}
	cout << "S kakogo goda?\n";
	cin >> k;
	bubble_sort(a.begin(), a.end());
	for(int i = 0; i < n; i++)
		if(a[i].year >= k) cout << a[i].name << ' ' << a[i].autor << '\n';
        return 0;
}

