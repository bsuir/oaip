#include <bits/stdc++.h>

using namespace std;

template<typename Input, typename Output>
Output Copy(Input first, Input last, Output d_first)
{
    while (first != last) {
        //cout << *first << ' ' << *last << '\n';
        *d_first++ = *first++;
    }
    return d_first;
}

int Max(vector<int> a)
{
	if(a.size() == 1) return a[0];
	if(a.size() == 2) return max(a[0], a[1]);
/*	cout << a.size() << ' ' << a[0] << '\n';
	for(int i = 0; i < a.size(); i++)
		cout << a[i] << ' ';
	cout << "\n\n";
*/	int x, y, n = a.size();
	vector<int> b, c;
	b.resize(n/2);
	c.resize(n - n/2);
	Copy(a.begin(), a.begin() + n/2, b.begin());
	Copy(a.begin()+ n/2, a.end(), c.begin());
	x = Max(b);
	y = Max(c);
	return max(x, y);
}

int main()
{
	int n, m;
	freopen("inp", "r", stdin);
	cin >> n;
	vector<int> a(n);
	for(int i = 0; i < n; i++)
		cin >> a[i];
	m = Max(a);
	cout << m;
	return 0;
}